import React from "react";
import { Outlet } from "react-router-dom";
import styles from "../../styles/goodsSection.module.scss";

export const GoodsSection = () => {
  return (
    <div className={styles.GoodsSection}>
      <Outlet />
    </div>
  );
};
