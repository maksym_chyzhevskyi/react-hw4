import React from "react";
import { Header } from "../components/header";
import { Main } from "../components/main";
import { MyModal } from "../components/myModal";
import styles from "../styles/layout.module.scss";

export const Layout = () => {
  return (
    <>
      <header className={styles.Header}>
        <div className={styles.container}>
          <Header />
        </div>
      </header>
      <main className={styles.Main}>
        <div className={styles.container}>
          <Main />
        </div>
      </main>
      <MyModal />
    </>
  );
};
