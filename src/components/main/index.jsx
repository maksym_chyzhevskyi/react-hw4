import React from "react";
import styles from "../../styles/main.module.scss";
import { GoodsSection } from "../goodsSection";

export const Main = () => {
  return (
    <div className={styles.Main__row}>
      <GoodsSection />
    </div>
  );
};
