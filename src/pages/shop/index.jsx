import React from "react";
import styles from "../../styles/shop.module.scss";
import { Product } from "../../components/product";
import { useSelector } from "react-redux";

const getProducts = (state) => state.products;

export const Shop = () => {
  const { error, status, items } = useSelector(getProducts);

  return (
    <div className={styles.Shop}>
      {error && <h2>{error}</h2>}

      {!error && status === "fulfilled" && (
        <ul className={styles.ShopList}>
          {items.map((product) => (
            <Product key={product.id} product={product}></Product>
          ))}
        </ul>
      )}
    </div>
  );
};
